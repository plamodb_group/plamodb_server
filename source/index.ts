// #region Environment

import dotenv from 'dotenv';
dotenv.config();

// #endregion Environment

// #region Imports

/* Express */
import express from 'express';
import {Express} from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

/* plamodb */
import {initializeDatabase} from './database';
import {attachHttpLogger, logInfo} from './utilities/logger';
import {apiRouter} from './routes';

// #endregion Imports

// #region Initialization

const application : Express = express();

attachHttpLogger(application);
initializeDatabase();

// #endregion Initialization

// #region Routes

application.use(cors());
application.use(bodyParser.json());
application.use('/api', apiRouter);

// #endregion Routes

// #region Life-Cycle

const port : number = (process.env.PORT ? Number(process.env.PORT) : 8080);
application.listen(port, () => {
  logInfo('plamo API server listening on port %d', port);
});

// #endregion Life-Cycle