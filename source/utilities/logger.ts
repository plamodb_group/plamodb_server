// #region Imports

/* Express */
import {Express} from 'express';

/* Pino */
import pino from 'pino';
import express_pino_logger from 'express-pino-logger';

// #endregion Imports

const logger = pino({
  name:       'plamodb', 
  level:      'debug',
  prettyPrint: !!process.env.DEVELOPMENT,
});

// #region Life-Cycle

/**
 * Attach an http logger to the specified express application.
 * 
 * @param application The application to attach the logger to.
 */
export function attachHttpLogger(application : Express) : void {
  application.use(express_pino_logger({ logger: logger }));
}

// #endregion Life-Cycle

// #region Logging

/**
 * Log an informational message.
 * 
 * @param message The message to log.
 * @param args The arguments to format the message with.
 */
export function logInfo(message : string, ...args : any[]) : void {
  logger.info(message, args);
}

// #endregion Logging