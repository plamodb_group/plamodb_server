// #region Imports

/* NodeJS */
import path from 'path';

/* SQLite */
import sqlite from 'sqlite3';

// #endregion Imports

// #region Constants

const DATABASE_FILE : string   = path.join(__dirname,  '../plamo.db');

// #endregion Constants

const sqlite3 = sqlite.verbose();
const database = new sqlite3.Database(DATABASE_FILE);

// #region Promise Wrappers

/**
 * Wrap the sqlite "all" operation in a promise.
 * 
 * @param query The query to execute.
 * @param parameters A list of query parameters.
 */
export function all(query : string, parameters? : any) : Promise<Array<any>> {
  return new Promise<Array<any>>((
    resolve : (rows : Array<any>) => void,
    reject : (reason : Error) => void
  ) => {
    database.all(
      query,
      parameters,
      function (error : Error, rows : Array<any>) {
        if (!error) {
          resolve(rows);
        } else {
          reject(error);
        }
      }
    );
  });
}

/**
 * Wrap the sqlite "get" operation in a promise.
 * 
 * @param query The query to execute.
 * @param parameters A list of query parameters.
 */
export function get(query : string, parameters? :any) : Promise<any> {
  return new Promise<any>((
    resolve : (row : any) => void,
    reject : (reason : Error) => void
  ) => {
    database.get(
      query,
      parameters,
      function (error : Error, row : any) {
        if (!error) {
          resolve(row);
        } else {
          reject(error);
        }
      }
    );
  });
}

/**
 * Wrap the sqlite "run" operation in a promise.
 * 
 * @param query The query to execute.
 * @param parameters A list of query parameters.
 */
export function run(query : string, parameters? : any) : Promise<number> {
  return new Promise<number>((
    resolve : (lastId : number) => void,
    reject : (reason : Error) => void
  ) => {
    database.run(
      query,
      parameters,
      function (error : Error) {
        if (!error) {
          resolve(this.lastID);
        } else {
          reject(error);
        }
      }
    )
  });
}

// #endregion Promise Wrappers