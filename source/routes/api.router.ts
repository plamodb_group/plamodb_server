// #region Imports

/* ExpressJS */
import express from 'express';
import {Router} from 'express';

/* plamodb */
import AssetsRouter from './assets.router';
import ModelsRouter from './models.router';

// #endregion Imports

const apiRouter : Router = express.Router();

apiRouter.use('/assets', AssetsRouter);
apiRouter.use('/models', ModelsRouter);

export {apiRouter};