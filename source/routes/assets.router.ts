// #region Imports

/* ExpressJS */
import express, {Request, Response, Router} from 'express';

/* NodeJS */
import path from 'path';
import fs from 'fs';

/* multer */
import multer, {StorageEngine} from 'multer';

/* http-status-codes */
import {StatusCodes, ReasonPhrases} from 'http-status-codes';

/* UUID */
import {v4} from 'uuid';

// #endregion Imports

// #region Express Configuration

const assetsRouter : Router = express.Router();
export default assetsRouter;

// #endregion Express Configuration

// #region Multer Configuration

const ASSET_FOLDER : string = path.join(__dirname,  '../assets/');
if (!fs.existsSync(ASSET_FOLDER)){
  fs.mkdirSync(ASSET_FOLDER);
}

const diskStorage : StorageEngine = multer.diskStorage({
  destination: (request : Request, file, callback) => callback(null, ASSET_FOLDER),
  filename: (request : Request, file, callback) => {
    const filename : string = v4();
    const extension : string = file.mimetype.split('/')[1];
    callback(null, `${filename}.${extension}`);
  }
});

const filter = (req : Request, file : any, callback : any) => {
  if (file.mimetype.startsWith('image')) {
    callback(null, true);
  } else {
    callback(null, false);
  }
};

const upload = multer({
  storage: diskStorage,
  fileFilter: filter
});

// #endregion Multer Configuration

/* View an asset. */
assetsRouter.use(express.static(ASSET_FOLDER));

// #region Avatars

/* Upload an avatar. */
assetsRouter.post(
  '/avatar',
  upload.single('avatar'),
  (request : Request, response : Response) => {
    response.status(StatusCodes.CREATED).send(request.file.filename);
  }
);

/* Delete an avatar. */
assetsRouter.delete(
  '/avatar/:asset',
  (request : Request, response : Response) => {
    let pathToAsset : string = path.join(ASSET_FOLDER, request.params.asset);
    fs.unlink(
      pathToAsset,
      (error : Error) => {
        if (!error) {
          response.status(StatusCodes.OK).send(ReasonPhrases.OK);
        } else {
          response.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error.message);
        }
      }
    )
  }
);

// #endregion Avatars