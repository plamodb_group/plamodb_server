// #region Imports

/* ExpressJS */
import express, {Request, Response, Router} from 'express';

/* http-status-codes */
import {StatusCodes, ReasonPhrases} from 'http-status-codes';

/* plamodb */
import {
  deleteModel,
  insertModel,
  selectModel, 
  selectModels,
  updateModel
} from '../database';

// #endregion Imports

const modelsRouter : Router = express.Router();
export default modelsRouter;

/* Get all models. */
modelsRouter.get(
  '/',
  async (request : Request, response : Response) => {
    let models : Array<any> = await selectModels();
    response.status(StatusCodes.OK).send(models);
  }
);

/* Get specific model. */
modelsRouter.get(
  '/:id',
  async (request : Request, response : Response) => {
    let model : any = await selectModel(Number(request.params.id));
    if (!!model) {
      response.status(StatusCodes.OK).send(model);
    } else {
      response.status(StatusCodes.NOT_FOUND).send(ReasonPhrases.NOT_FOUND);
    }
  }
);

/* Create a new model. */
modelsRouter.post(
  '/',
  async (request : Request, response : Response) => {
    let newModelId : number = await insertModel();
    let newModel : any = await selectModel(newModelId);
    response.status(StatusCodes.CREATED).location(`${newModelId}`).send(newModel);
  }
);

/* Update a specific model. */
modelsRouter.put(
  '/:id',
  async (request : Request, response : Response) => {
    await updateModel(Number(request.params.id), request.body);
    response.status(StatusCodes.OK).send(ReasonPhrases.OK);
  }
);

/* Delete a specific model. */
modelsRouter.delete(
  '/:id',
  async (request : Request, response : Response) => {
    await deleteModel(Number(request.params.id));
    response.status(StatusCodes.OK).send(ReasonPhrases.OK);
  }
);