// #region Imports

/* plamodb */
import {logInfo} from '../utilities/logger';
import {createModelsTable} from './models.database';

// #endregion Imports

export * from './models.database';

/**
 * Initialize the database, including ensuring that all required tables are
 * present.
 */
export function initializeDatabase() {
  logInfo('Intitializing database.');
  createModelsTable();
}