// #region Imports

/* plamodb */
import {all, get, run} from '../utilities/database';

// #endregion Imports

// #region Operations

/**
 * Create the table for the models.
 */
export async function createModelsTable() : Promise<number> {
  let query : string = 
    `CREATE TABLE IF NOT EXISTS models (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT,
      caption TEXT,
      avatar TEXT,
      releasedOn TEXT DEFAULT CURRENT_DATE,
      description TEXT,
      draft INTEGER DEFAULT 1
    );`;

  return run(query);
}

/**
 * Query the database for a set of models.
 * 
 * TODO: Add search parameters.
 */
export async function selectModels() : Promise<Array<any>> {
  let query : string =
    `
      SELECT * FROM models;
    `;

  return all(query);
}

/**
 * Query the database for specific model.
 * 
 * @param id The identifier of the model to search for.
 */
export async function selectModel(id : number) : Promise<any> {
  let query : string = 
    `
      SELECT * FROM models
      WHERE id = ?
    `;

  return get(query, [id]);
}

/**
 * Insert a new model into the database.
 */
export async function insertModel() : Promise<number> {
  let query : string =
    `
      INSERT INTO models
      DEFAULT VALUES
    `;

  return run(query);
}

/**
 * Update an existing model in the database.
 * 
 * @param id The identifier of the model to update.
 * @param model The data to update the model with.
 */
export async function updateModel(id : number, model : any) : Promise<number> {
  let query : string = 
    `
      UPDATE models
      SET
        name = ?,
        caption = ?,
        avatar = ?,
        description = ?,
        releasedOn = ?
      WHERE id = ? 
    `;
  
  return run(
    query, 
    [
      model.name, 
      model.caption,
      model.avatar,
      model.description, 
      model.releasedOn, 
      id
    ]
  );
}

/**
 * Delete an existing model from the database.
 * 
 * @param id The identifier of the model to delete.
 */
export async function deleteModel(id : number) : Promise<number> {
  let query : string = 
    `
      DELETE FROM models
      WHERE id = ?
    `;

  return run(
    query,
    [
      id
    ]
  );
}

// #endregion Operations